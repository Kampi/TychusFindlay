import sc2

from enum import Enum
from sc2.constants import *

class BuildOrderIndices(Enum):
    BUILDING = 0
    CONDITION = 1
    POSITION = 2

# Specify the build order for the bot
BUILDORDER = {
    sc2.Race.Terran : [
        [BARRACKS, 15, None],
        [BARRACKS, 16, None],
        [BARRACKS, 17, None],
        [BARRACKS, 18, None],
    ],

    sc2.Race.Protoss : [

    ],

    sc2.Race.Zerg : [


    ]
}   