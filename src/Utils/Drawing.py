import cv2
import numpy as np

import Data.GlobalData as Global

class Drawing(object):
    # Not used anymore
    """
    def DrawOwnUnitHP(self, Image):
        for Unit in self.units():
            Radius = 0
            Position = Unit.position
            Health = Unit.health_percentage

            if(Unit.is_structure):
                Radius = 8
            else:
                Radius = 3

            # Calculate the color with the health
            Color = (0, int(255 - ((1.0 - Health) * 255)), int((1.0 - Health) * 255))
            cv2.circle(Image, (int(Position[0]), int(Position[1])), Radius, Color, -1)

    def DrawnEnemyUnitHP(self, Image):
        for Unit in self.known_enemy_units:
            if Unit.is_visible:
                Position = Unit.position
                Health = Unit.health_percentage

                # Calculate the color with the health
                Color = (255, int(255 - ((1.0 - Health) * 255)), int((1.0 - Health) * 255))
                cv2.circle(Image, (int(Position[0]), int(Position[1])), 3, Color, -1)

        for Building in self.known_enemy_structures:
            if Building.is_visible:
                Position = Building.position
                Health = Building.health_percentage

                # Calculate the color with the health
                Color = (255, int(255 - ((1.0 - Health) * 255)), int((1.0 - Health) * 255))
                cv2.circle(Image, (int(Position[0]), int(Position[1])), 8, Color, -1)
    """

    def DrawOwnUnits(self, Image):
        """
        Draw the units and buildings of the bot
    
        Arguments:
            Image {numpy.array} -- Input image where the scout informations should be drawn
        """

        # Loop over each unit and draw them
        for Unit in self.units():
            if(Unit.is_ready):
                if(Unit.type_id in Global.UNITDRAWINGS):
                    Position = Unit.position
                    cv2.circle(
                        Image,
                        (int(Position[0]), int(Position[1])),
                        Global.UNITDRAWINGS[Unit.type_id][0],
                        Global.UNITDRAWINGS[Unit.type_id][1],
                        -1,
                    )


    def DrawEnemyBuildings(self, Image, ScoutInfos):
        """
        Draw the scouted buildings
        NOTE: Use a scouting dictionary, because self.__known_units doesn´t give the buildings under construction
    
        Arguments:
            Image {numpy.array} -- Input image where the scout informations should be drawn
            ScoutInfos {dict} -- Scouting informations
        """

        if("Units" in ScoutInfos):
            for EnemyBuilding in ScoutInfos["Units"]:
                if((EnemyBuilding.type_id in Global.UNITDRAWINGS) and EnemyBuilding.is_structure):
                    Position = EnemyBuilding.position
                    cv2.circle(
                        Image,
                        (int(Position[0]), int(Position[1])),
                        Global.UNITDRAWINGS[EnemyBuilding.type_id][0],
                        Global.UNITDRAWINGS[EnemyBuilding.type_id][1],
                        -1,
                    )

    def DrawEnemyUnits(self, Image):
        """
        Draw the enemy units

        Arguments:
            Image {numpy.array} -- Input image where the scout informations should be drawn
        """

        for Unit in self.known_enemy_units:
            if(Unit.type_id in Global.UNITDRAWINGS):
                Position = Unit.position
                cv2.circle(
                    Image,
                    (int(Position[0]), int(Position[1])),
                    Global.UNITDRAWINGS[Unit.type_id][0],
                    Global.UNITDRAWINGS[Unit.type_id][1],
                    -1,
                ) 

    def DrawImages(self, Images, ScaleX = 2, ScaleY = 2, Title = "Intel"):
        """
        Draw the given images

        Arguments:
            Image {list(numpy.array)} -- List with input images
        
        Keyword Arguments:
            ScaleX {int} -- x scale of the final image (default: {2})
            ScaleY {int} -- y scale of the final image(default: {2})
            Title {str} -- Title of the window (default: {"Intel"})
        """

        Image = None
        for Key in Images:
            Flipped = cv2.flip(Images[Key], 0)
            Resized = cv2.resize(Flipped, dsize = None, fx = ScaleX, fy = ScaleY)

            if(Image is None):
                Image = Resized
            else:
                Image = np.concatenate((Image, Resized), axis=1)

        cv2.imshow(Title, Image)
        cv2.waitKey(1)