import os
import sc2
import math
import datetime

from sc2.ids.unit_typeid import *
from colorama import init, Fore, Style

import Data.GlobalData as Global

class HelpFunctions(object):
    # Init colorama
    init()

    @staticmethod
    def CirclePoints(Center, Steps = 8, Radius = 8, Point = None, Digits = 15):
        """
        Get a set or a single point of positions on a circle path

        Arguments:
            Center {sc2.position.Point2} -- Center of the circle

        Keyword Arguments:
            Steps {int} -- Steps for the circle path (default: {8})
            Radius {int} -- Radius of the cicle (default: {8})
            Digits {int} -- Digits to round (default: {15})
            Point {int} -- Return a single point on the circle (default: {None})

        Returns:
            {sc2.position.Point2} -- Set or point with coordinates on the circle path
        """
        if(Point is None):
            Return = {
                        sc2.position.Point2(
                            (
                                round(Center.x + math.sin(2 * math.pi / Steps * Step) * Radius, Digits),
                                round(Center.y + math.cos(2 * math.pi / Steps * Step) * Radius, Digits),
                            )
                        )
                        for Step in range(Steps)
                    }
        else:
            Return = sc2.position.Point2(
                sc2.position.Pointlike((
                    round(Radius * math.sin(2 * math.pi * Point / Steps) + Center[0]), 
                    round(Radius * math.cos(2 * math.pi * Point / Steps) + Center[1])
                ))
            )

        return Return

    @staticmethod
    def ReserveUnit(Unit, Type = "Worker"):
        """
        Set a unit as a reserved unit
        
        Arguments:
            Unit {sc2.unit.Unit} -- Unit which should be reserved
        
        Keyword Arguments:
            Type {str} -- Unit group (default: {"Worker"})
        """

        if Type in Global.RESERVEDUNITS:
            if type(Unit) == sc2.unit.Unit:
                Global.RESERVEDUNITS[Type].append(Unit.tag)

    @staticmethod
    def FreeUnit(Unit, Type = "Worker"):
        """
        Unset a reserved unit
        
        Arguments:
            Unit {sc2.unit.Unit} -- Reserved unit which should be released
        
        Keyword Arguments:
            Type {str} -- Unit group (default: {"Worker"})
        """

        if Type in Global.RESERVEDUNITS:
            if type(Unit) == sc2.unit.Unit:
                if Unit.tag in Global.RESERVEDUNITS[Type]:
                    Global.RESERVEDUNITS[Type].remove(Unit.tag)

    @staticmethod
    def Str2Bool(String):
        """
        Convert a string to a boolean value

        Arguments:
            object {str} -- Input string
        
        Returns:
            [bool] -- Boolean representation of the input string
        """

        if(String.lower() in ("yes", "true", "t", "y", "1")):
            return True
        else:
            return False

    @staticmethod
    def PrintDebugMessage(Message):
        """
        Print a debug message in the console
        
        Arguments:
            Message {str} -- Message
        """

        if __debug__:
            print(Fore.YELLOW + f"{datetime.datetime.now().time()} - [DEBUG] {Message}" + Style.RESET_ALL)
        else:
            print(Fore.RED + "[DEBUG] No debug mode enabled!" + Style.RESET_ALL)

    @staticmethod
    def PrintToLog(Message, Path):
        """
        Write a message into a log
        
        Arguments:
            Message {str} -- Message
            Path {str} -- Path + name of the logfile
        """

        Path = Path.rsplit("/", 1)
        FilePath = Path[0]
    
        # Check if the path exist
        if not os.path.exists(FilePath):
            os.makedirs(FilePath)

        with open(Path, "a") as File:
            File.write(Message + "\n")

    @staticmethod
    def GetSupplyDepots(Units):
        """
        Get all supply depots from a given unit list
        
        Arguments:
            Units {sc2.unit.Units} -- List with sc2 units
        
        Returns:
            [sc2.unit.Units] -- List with all supply depots
        """

        return Units.filter(pred = lambda x: x.type_id == SUPPLYDEPOT or x.type_id == SUPPLYDEPOTLOWERED)

    @staticmethod
    def SplashUnits(Units):
        """

        Arguments:
            units {[type]} -- [description]
        
        Returns:
            [type] -- [description]
        """

        return Units.subgroup([x for x in Units if x.type_id in Global.SPLASH])

    @staticmethod
    def GetMeleeUnits(Units):
        """
        Get all melee units.
        NOTE: Ultralisk has range 1

        Arguments:
            units {sc2.unit.Units} -- List with sc2 units
        
        Returns:
            [sc2.unit.Unit] -- List with melee units
        """

        return Units.filter(lambda x: x.ground_range <= 1)

    @staticmethod
    def PickRandomWorker(Position, Units):
        """
        Pick a random worker from a list of given units
        
        Arguments:
            Position {sc2.position.Point2D} -- Prefered position
            Units {sc2.unit.Units} -- List with sc2 units
        
        Returns:
            sc2.unit.Unit -- Selected worker
        """
        
        # Choose a new idle worker, next to the building
        NewWorker = Units.tags_not_in(Global.RESERVEDUNITS["Worker"]).prefer_close_to(Position).filter(pred = lambda x: not(x.is_carrying_minerals or x.is_carrying_vespene) and x.is_idle)

        # Choose a new idle worker from everywhere if no worker is found
        if len(NewWorker) == 0:
            NewWorker = Units.tags_not_in(Global.RESERVEDUNITS["Worker"]).prefer_idle.filter(pred = lambda x: not(x.is_carrying_minerals or x.is_carrying_vespene))

        return NewWorker[0]