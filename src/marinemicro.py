import heapq

from sc2.ids.ability_id import EFFECT_STIM_MARINE
from sc2.ids.buff_id import STIMPACK
from sc2.ids.unit_typeid import MARINE

import Utils.HelpFunctions as Help


class MarineMicro:
    def __init__(self):
        self.combinedActions = []

    def use_stim(self, marine):
        """
        Use stim pack for a unit
        
        Arguments:
            marine {[type]} -- [description]
        """

        if not marine.has_buff(STIMPACK) and marine.health > 10:
            self.combinedActions.append(marine(EFFECT_STIM_MARINE))

    def attack(self, marine, units):
        if Help.SplashUnits(units):
            self.attack_lowHP(marine, Help.SplashUnits(units))
        elif Help.GetMeleeUnits(units):
            self.attack_lowHP(marine, Help.GetMeleeUnits(units))
        else:
            self.attack_lowHP(marine, units)

    def attack_lowHP(self, marine, units):
        lowesthp = min(unit.health for unit in units)
        lowEnemies = units.filter(lambda x: x.health == lowesthp)
        target = lowEnemies.closest_to(marine)
        self.combinedActions.append(marine.attack(target))

    def micro5(self, marine, units):
        """Controls the micro of marines in range 0-5 to a hostile unit."""
        meleeEnemies = Help.GetMeleeUnits(units)
        splashEnemies = Help.SplashUnits(units)
        # 16 directions to run to, filter out unreachable points
        circle = Help.CirclePoints(Center=marine.position, Steps=16, Radius=1, Digits=15)
        retreatPoints = {p for p in circle if self._game_info.pathing_grid[(p.rounded)] == 0}

        if self.units(MARINE).owned.amount < 2:
            closestEnemy = units.closest_to(marine)
            return min(retreatPoints, key=lambda x: abs(x.distance_to(closestEnemy) - 5))
        nextMarine = self.units(MARINE).owned.filter(lambda x: x != marine).closest_to(marine)

        if not meleeEnemies and not splashEnemies:
            closestEnemy = units.closest_to(marine)
            retreatPointsChoice = heapq.nlargest(6, retreatPoints, key=lambda x: abs(x.distance_to(closestEnemy)) - 3)
            retreatPoint = min(retreatPointsChoice, key=lambda x: abs(x.distance_to(nextMarine) - 3))

        elif not meleeEnemies and splashEnemies:
            closestSplashEnemy = splashEnemies.closest_to(marine)
            retreatPointsChoice = heapq.nlargest(
                6, retreatPoints, key=lambda x: abs(x.distance_to(closestSplashEnemy)) - 5
            )
            retreatPoint = min(retreatPointsChoice, key=lambda x: abs(x.distance_to(nextMarine) - 3))

        elif meleeEnemies and not splashEnemies:
            closestEnemy = meleeEnemies.closest_to(marine)
            retreatPoint = min(retreatPoints, key=lambda x: abs(x.distance_to(closestEnemy) - 5))

        elif meleeEnemies and splashEnemies:
            closestSplashEnemy = splashEnemies.closest_to(marine)
            retreatPointsChoice = heapq.nlargest(2, retreatPoints, key=lambda x: x.distance_to(closestSplashEnemy))
            retreatPoint = min(retreatPointsChoice, key=lambda x: abs(x.distance_to(nextMarine) - 3))

        return retreatPoint

    def micro15(self, marine, units):
        """Controls the micro of marines in range 5-15 to a hostile unit."""
        splashEnemies = Help.SplashUnits(units)
        # 16 directions to run to, filter out unreachable points
        circle = Help.CirclePoints(Center=marine.position, Steps=16, Radius=1, Digits=15)
        retreatPoints = {p for p in circle if self._game_info.pathing_grid[(p.rounded)] == 0}
        if not splashEnemies or self.units(MARINE).owned.amount < 2:
            closestEnemy = units.closest_to(marine)
            return min(retreatPoints, key=lambda x: x.distance_to(closestEnemy))

        nextMarine = self.units(MARINE).owned.filter(lambda x: x != marine).closest_to(marine)
        behind_friend = units.closest_to(marine).distance_to(nextMarine) < units.closest_to(marine).distance_to(marine)

        if splashEnemies:
            closestEnemy = splashEnemies.closest_to(marine)
            choices = 7 if not closestEnemy.is_attacking or behind_friend else 12
            retreatPointsChoice = heapq.nsmallest(choices, retreatPoints, key=lambda x: x.distance_to(closestEnemy))
            return min(retreatPointsChoice, key=lambda x: abs(x.distance_to(nextMarine) - 3))

    async def micro(self, armytarget=None):
        self.combinedActions = []
        # continues used to get to the next marine after order was given
        for marine in self.units(MARINE).owned:
            nearEnemies = self.known_enemy_units.not_structure.closer_than(15, marine)
            if nearEnemies:
                inrangeEnemies = nearEnemies.closer_than(5, marine)
                if inrangeEnemies:
                    # stim
                    self.use_stim(marine)  # no continue needed (?)
                    # weapon not on cooldown, attack
                    if marine.weapon_cooldown == 0:
                        self.attack(marine, inrangeEnemies)
                        continue
                    # move,weapon on cooldown
                    else:
                        retreatPoint = self.micro5(marine, inrangeEnemies)
                        self.combinedActions.append(marine.move(retreatPoint))
                        continue
                else:
                    # no enemy in marine range, move
                    retreatPoint = self.micro15(marine, nearEnemies)
                    self.combinedActions.append(marine.move(retreatPoint))
                    continue

            else:
                # no enemy in range 15, move to target position
                if armytarget:
                    self.combinedActions.append(marine.move(armytarget))
                    continue
                else:
                    # no enemy in range 15 and no target position, attack other known enemies
                    allEnemyUnits = self.known_enemy_units
                    if allEnemyUnits:
                        closestEnemy = allEnemyUnits.closest_to(marine)
                        self.combinedActions.append(marine.attack(closestEnemy))
                        continue
                    # move to enemy start location if no enemy can be seen
                    else:
                        self.combinedActions.append(marine.move(self.enemy_start_locations[0]))
                        continue
        # do actions that were collected in the for loop
        await self.do_actions(self.combinedActions)
