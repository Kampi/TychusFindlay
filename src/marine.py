
import sc2
import sc2.constants as const
from sc2 import Difficulty, Race
from sc2.player import Bot, Computer

from marinemicro import MarineMicro


class m1crob0t(sc2.BotAI, MarineMicro):
    def __init__(self):
        self.combinedActions = []
        self.leavegame = False
        self.testresult = 0

    async def on_step(self, iteration):
        if self.units(const.MARINE).owned.amount > 0:
            await self.micro()
        if self.leavegame:
            await self._client.leave()
            if __name__ == "__main__":
                raise SystemExit
            # pass
        else:
            if not self.units(const.MARINE).owned:
                enemies_left = self.known_enemy_units.not_structure.amount
                print(f"============\nGAME ENDED\nBOT lost with {enemies_left} Enemies left.\n============")
                self.testresult = -enemies_left
                self.leavegame = True

            if not self.known_enemy_units.not_structure and self.units(const.MARINE).owned:
                marines_left = self.units(const.MARINE).owned.amount
                print(f"============\nGAME ENDED\nBOT won with {marines_left} Marines left.\n============")
                self.testresult = marines_left
                self.leavegame = True


def main():
    # sc2.run_game(
    #     sc2.maps.get("test_ling_ultra"),
    #     [Bot(Race.Terran, m1crob0t()), Computer(Race.Zerg, Difficulty.VeryHard)],
    #     realtime=False,
    # )
    sc2.run_game(
        sc2.maps.get("test_ling_ultra"),
        [Bot(Race.Terran, m1crob0t()), Computer(Race.Zerg, Difficulty.CheatInsane)],
        realtime=True,
    )


if __name__ == "__main__":
    main()
