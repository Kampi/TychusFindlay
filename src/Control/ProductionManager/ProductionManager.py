import sc2

from sc2.constants import *

import Utils.HelpFunctions as Help
import Data.GlobalData as Global

from . import BuildingManager
from . import UnitManager
from . import UpgradeManager
from Control.AbilityManager import AbilityManager as AbilityManager

class ProductionManager(BuildingManager, UnitManager, UpgradeManager, AbilityManager):
    def __init__(self):
        super().__init__()

    def Init(self):
        """
        Initialize the manager
        """

        super().Init()

        Help.PrintDebugMessage("Initialize ProductionManager")

    async def ManagerIteration(self):
        """

        """

        await super().Process()
        await super().ManagerIteration()