import sc2

from sc2.constants import *

import Data.GlobalData as Global
import Utils.HelpFunctions as Help

class UpgradeManager(sc2.BotAI):
    """
    This class handles the research of upgrades
    """

    def __init__(self):
        super().__init__()

        self.__UpgradeQueue = list()

    def Init(self):
        """
        Initialize the manager
        """
        
        super().Init()

        Help.PrintDebugMessage("Initialize UpgreadeManager")

    def AddUpgradeToQueue(self, Upgrade, Building, Priority = "Low"):
        pass

    async def Process(self):
        """
        Handle research of the upgrades in the queue
        """

        await self.ProcessUpgradeQueue()

    async def ProcessUpgradeQueue(self):
        pass
