import sc2

from enum import Enum
from sc2.constants import *
from operator import itemgetter

import BuildOrder
import Data.GlobalData as Global
import Utils.HelpFunctions as Help

# Enum definitions for this manager
class QueueIndices(Enum):
    BUILDING = 0
    POSITION = 1

# NOTE: Try to use this for every race!
class BuildingManager(sc2.BotAI):
    """
    This class handles the construction and the usage of all buildings
    """

    def __init__(self):
        super().__init__()
        self.__BuildQueue = list()
        self.__BuildOrder = None

    def Init(self):
        """
        Initialize the manager
        """

        super().Init()

        Help.PrintDebugMessage("Initialize BuildingManager")

        if self.race == sc2.Race.Protoss:
            self.__Worker = PROBE
            self.__TownHall = NEXUS
            self.__Gas = ASSIMILATOR
        elif self.race == sc2.Race.Terran:
            self.__Worker = SCV
            self.__TownHall = COMMANDCENTER
            self.__Gas = REFINERY
        else:
            self.__Worker = DRONE
            self.__TownHall = HATCHERY
            self.__Gas = EXTRACTOR

    def AddBuildingToQueue(self, Building, Position = None, Priority = "Low"):
        """
        Add a new building to queue

        Arguments:
            Building {sc2.unit.Unit} -- Unit type of the building
            Position {list} -- Position of the building
        
        Keyword Arguments:
            Priority {str} -- Priority for the new building. Set to "High" for important buildings (default: {"Low"})
        """

        NotInQueue = True

        # Check if the queue contains a building for the given position
        for NewBuilding in self.__BuildQueue:
            if Position == NewBuilding[QueueIndices.POSITION.value]:
                NotInQueue = False

        # TODO
        # Chose a random position in the mainbase if no position is given
        if Position is None:
            pass

        if NotInQueue:
            print(f"[GAME] Add {Building}...")
            if Priority == "Low":
               self.__BuildQueue.append([Building.value, Position])
            else:
                # Only insert if a list item exist
                if len(self.__BuildQueue) > 0:
                   self.__BuildQueue.insert(0, [Building.value, Position])
                else:
                    self.__BuildQueue.append([Building.value, Position])

    def RemoveBuildingFromQueue(self, Building):
        """
        Remove a building from building queue
        
        Arguments:
            Building {sc2.unit.Unit} -- Building which should be removed
        """

        for Item in self.__BuildQueue:
            if Item[QueueIndices.BUILDING.value] == Building.value:
                self.__BuildQueue.remove(Item)

    async def Process(self):
        """
        Handle the buildings in the building queue
        """

        await super().Process()
        await self.BuildSupply()        

        if self.__BuildOrder is None:
            print(f"[GAME] Use {self.race} build order")
            self.__BuildOrder = BuildOrder.BUILDORDER[self.race]

            # Sort the buildorder based on the supply 
            #self.__BuildOrder = sorted(self.__BuildOrder, itemgetter = BuildOrder.BuildOrderIndices.CONDITION.value)

        # Get the first item of the build order
        if len(self.__BuildOrder) > 0:
            Item = self.__BuildOrder[0]
        
            # Check if the build condition is fulfilled
            if self.supply_used >= Item[BuildOrder.BuildOrderIndices.CONDITION.value]:
                # Read the position from the build order
                Position = Item[BuildOrder.BuildOrderIndices.POSITION.value]

                # Use the first baracks for ramp
                if Help.GetSupplyDepots(self.units).ready.exists:
                    if await self.can_place(BARRACKS, self.main_base_ramp.barracks_in_middle):
                        Position = self.main_base_ramp.barracks_in_middle
                    # Choose a random position if the given position is none
                    elif Position is None:
                        Position = Help.GetSupplyDepots(self.units).random.position

                    self.AddBuildingToQueue(Item[BuildOrder.BuildOrderIndices.BUILDING.value], Position)

                    # Remove the handled item
                    self.__BuildOrder.remove(Item)

        # Execute the build queue
        await self.ProcessBuildingQueue()

    async def ProcessBuildingQueue(self):
        """
        Process the buildings from the building queue
        NOTE: Structure of the queue:
            [[Building, Position, Handled], ...]
        """
        
        # Get the first item of the build queue
        if self.__BuildQueue:
            Building = self.__BuildQueue[0]

            # Get the building enum
            NewBuilding = sc2.ids.unit_typeid.UnitTypeId(Building[QueueIndices.BUILDING.value])

            if self.can_afford(NewBuilding):
                print(f"[GAME] Construct {NewBuilding.name}...")

                # Construct the building if it can be placed
                if await self.can_place(NewBuilding, Building[QueueIndices.POSITION.value]):
                    Position = Building[QueueIndices.POSITION.value]
                # Choose a position next to the original place
                else:
                    Position = await self.find_placement(NewBuilding, Building[QueueIndices.POSITION.value])
                
                # Pick a worker next to the build position. Ignore reserved worker
                BuildingWorker = Help.PickRandomWorker(Position = Position, Units = self.units(self.__Worker))

                # TODO: Worker muss Minerals vorm Bauen abliefern

                # Pick a new worker if the worker is idle
                if not BuildingWorker.is_idle:
                    BuildingWorker = Help.PickRandomWorker(Position = Position, Units = self.units(self.__Worker))

                # Construct the building
                await self.do(BuildingWorker.build(NewBuilding, Position))

                # Remove all handled buildings
                self.__BuildQueue.remove(Building)

    async def BuildSupply(self, Position = 0):
        """        
        Handle the construction of the supply

        Keyword Arguments:
            Position {int} -- Optional parameter for depot position (default: {0})
        """

        if self.race == sc2.Race.Terran and (self.supply_left < 5) and (not self.already_pending(SUPPLYDEPOT)):
            # Place the first and the second depot at the ramp
            if await self.can_place(SUPPLYDEPOT, list(self.main_base_ramp.corner_depots)[0]):
                self.AddBuildingToQueue(SUPPLYDEPOT, list(self.main_base_ramp.corner_depots)[0])
            elif await self.can_place(SUPPLYDEPOT, list(self.main_base_ramp.corner_depots)[1]):
                self.AddBuildingToQueue(SUPPLYDEPOT, list(self.main_base_ramp.corner_depots)[1])
            else:
                # Place all other depots at random positions
                if Help.GetSupplyDepots(self.units).ready.exists:
                    Depot = Help.GetSupplyDepots(self.units).ready.random
                    self.AddBuildingToQueue(SUPPLYDEPOT, Depot.position.random_on_distance(10))

        elif self.race == sc2.Race.Protoss:
            if (self.supply_left < 5) and (not self.already_pending(PYLON)):
                Nexuses = self.units(NEXUS).ready
                if Nexuses.exists and self.can_afford(PYLON):
                    Worker = self.select_build_worker(pos=Nexuses.first.position)
                    x = (Nexuses.first.position).towards(self.game_info.map_center, 8)
                    await self.do(Worker.build(PYLON, x))
        else:
            Supply = OVERLORD

    async def BuildGas(self, TownHall, GasCount = 1):
        """
        Handle the construction for gas ressource

        Arguments:
            TownHall {Unit} -- Select the townhall where the gas should be constructed
            GasCount {list} -- Number of gas which should be constructed (default: {1})
        """
    
        if(TownHall.exist):
            # Get the position for the gas next to the townhall
            for Vespene in self.state.vespene_geyser.closer_than(15.0, TownHall).take(GasCount):
                if not self.can_afford(ASSIMILATOR):
                    break

                # Get a worker next to the Vespene geyser
                Worker = self.select_build_worker(Vespene.position)
                if Worker is None:
                    break

                # Construct, if geyser is not used already
                if not self.units(self.__Gas).closer_than(1.0, Vespene).exists:
                    await self.do(Worker.build(self.__Gas, Vespene))

    async def Expand(self):
        """
        Handle the expansions
        """

        Position = await self.get_next_expansion()

    async def UpgradeCommandCenter(self, CommandCenter, Upgrade):
        if CommandCenter.ready.exists and self.can_afford(Upgrade):
            pass

    async def UpgradeHachery(self, Hatchery, Upgrade):
        pass