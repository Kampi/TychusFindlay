import sc2

from enum import Enum
from sc2.constants import *

import Data.GlobalData as Global
import Utils.HelpFunctions as Help

class QueueIndices(Enum):
    UNITID = 0
    UNITCOUNT = 1
    TRAINBUILDINGS = 2

class UnitManager(sc2.BotAI):
    """
    This class handles the unit production
    """

    def __init__(self):
        super().__init__()
    
        self.__UnitQueue = list()

    def Init(self):
        """
        Initialize the manager
        """

        super().Init()

        Help.PrintDebugMessage("Initialize UnitManager")

        if self.race == sc2.Race.Terran:
            self.__Worker = SCV
        elif self.race == sc2.Race.Protoss:
            self.__Worker = PROBE
        else:
            self.__Worker = DRONE

    def AddUnitsToQueue(self, Unit, Count, TrainBuildings, Priority = "Low"):
        """
        Add new units to the build queue
        
        Arguments:
            Unit {sc2.unit.Unit} -- New unit
            Count {int} -- Unitcount
            TrainBuildings {sc.unit.Unit} -- Train buildings for the units
        
        Keyword Arguments:
            Priority {str} -- Priority for the units (default: {"Low"})
        """

        NotInQueue = True
        Index = 0

        # Check if the queue contains a units with the same name
        for NewUnit in self.__UnitQueue:
            if Unit.value == NewUnit[QueueIndices.UNITID.value]:
                NotInQueue = False
            Index += 1

        if Count > 0:
            print(f"[GAME] Add {Count}x {Unit}...")
            if Priority == "Low":
                if NotInQueue:
                    self.__UnitQueue.append([Unit.value, Count, TrainBuildings])
                else:
                    self.__UnitQueue[Index - 1][QueueIndices.UNITCOUNT.value] += Count
            else:
                # Only insert if a list item exist
                if len(self.__UnitQueue) > 0:
                    self.__UnitQueue.insert(0, [Unit.value, Count, TrainBuildings])
                else:
                    self.__UnitQueue.append([Unit.value, Count, TrainBuildings])

    def RemoveUnitFromQueue(self, Unit, Count):
        """
        Removes units from the unit queue
        
        Arguments:
            Unit {sc2.unit.Unit} -- Unit which should be removed
            Count {int} -- Units to remove. Use 0 to remove all
        """

        for UnitQueue in self.__UnitQueue:
            # Remove all units if the "count" parameter is zero
            if Count == 0:
                self.__UnitQueue.remove(UnitQueue)
            else:
                # Otherwise decrease the unit count
                if UnitQueue[QueueIndices.UNITID.value] == Unit.value:
                    UnitQueue[QueueIndices.UNITCOUNT.value] -= Count

                # Remove the element if the count is 0 or less
                if UnitQueue[QueueIndices.UNITCOUNT.value] <= 0:
                    self.__UnitQueue.remove(UnitQueue)

    def GetTownHalls(self):
        """
        Get the town halls of a specific race
        
        Returns:
            [sc2.unit.Units] -- List with all town halls of the given race
        """

        if self.race == sc2.Race.Terran:
            Expression = lambda x: x.type_id == COMMANDCENTER or x.type_id == ORBITALCOMMAND or x.type_id == PLANETARYFORTRESS 
        elif self.race == sc2.Race.Protoss:
            Expression = lambda x: x.type_id == NEXUS
        else:
            Expression = lambda x: x.type_id == HATCHERY or x.type_id == LAIR or x.type_id == HIVE 

        return self.units.filter(pred = Expression)

    async def Process(self):
        """
        Process the unit production
        """

        await super().Process()
        await self.BuildWorker()

        Buildings = self.units(BARRACKS).ready.noqueue

        # Add a worker for each empty town hall
        self.AddUnitsToQueue(MARINE, len(Buildings), Buildings,)
        await self.ProcessUnitQueue()

    async def ProcessUnitQueue(self):
        """
        Handle the training of the units in the unit queue
        NOTE: Structure of the queue:
            [Unit, Count, [TrainingBuildings]], ...]
        """

        # Get the first item of the build order
        if len(self.__UnitQueue) > 0:
            Unit = self.__UnitQueue[0]
            NewUnit = sc2.ids.unit_typeid.UnitTypeId(Unit[QueueIndices.UNITID.value])

            # Loop over each building
            for TrainingBuilding in Unit[QueueIndices.TRAINBUILDINGS.value]:
                # Get the training building by his tag
                Building = self.units.find_by_tag(TrainingBuilding.tag)        

                # Train the unit
                if not Building is None:
                    if self.can_afford(NewUnit) and Unit[QueueIndices.UNITCOUNT.value] > 0:
                        print(f"[GAME] Train {sc2.ids.unit_typeid.UnitTypeId(Unit[QueueIndices.UNITID.value])} in {TrainingBuilding.name}...")
                        Unit[QueueIndices.UNITCOUNT.value] -= 1
                        await self.do(Building.train(NewUnit))

                        # Remove the unit from queue
                        if(Unit in self.__UnitQueue):
                            self.__UnitQueue.remove(Unit)

    async def BuildWorker(self):
        """
        Handle worker training
        """

        # TODO: Nicht noch einmal überarbeiten? Weil der erst wieder mit Workern anfängt, sobald die Expansion steht
        # Ggf. möchte man aber schon vorher Worker dafür bauen?
        optimal_count = Global.WORKER_PER_MINERALS * len(self.GetTownHalls()) + 3 * len(self.units(REFINERY))
        if (len(self.units(self.__Worker)) < min(optimal_count, Global.MAX_WORKERS)) and self.can_afford(self.__Worker):
            # Get all idle buildings
            Buildings = self.GetTownHalls().ready.noqueue

            # Add a worker for each empty town hall
            self.AddUnitsToQueue(self.__Worker, len(Buildings), Buildings, Priority = "High")

    async def BuildMilitary(self):
        """
        """

        pass