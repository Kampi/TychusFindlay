import sc2

from sc2.constants import *

import Data.GlobalData as Global
import Utils.HelpFunctions as Help

class AbilityManager(sc2.BotAI):
    def __init__(self):
        super().__init__()

    def Init(self):
        """
        Initialize the manager
        """

        super().Init()

        Help.PrintDebugMessage("Initialize AbilityManager")

    async def Process(self):
        await self.DepotControl()

    async def DepotControl(self):
        """
        Handle the state of terran supply depots
        """

        for Depot in self.units(SUPPLYDEPOT).ready:
            for unit in self.known_enemy_units.not_structure:
                # Raise depots when enemies are nearby
                if unit.position.to2.distance_to(Depot.position.to2) < 15:
                    await self.do(Depot(MORPH_SUPPLYDEPOT_RAISE))
            else:
                await self.do(Depot(MORPH_SUPPLYDEPOT_LOWER))

    async def UseScan(self, Position):
        """
        """

        if self.race == sc2.Race.Terran:
            for Orbital in self.units(ORBITALCOMMAND).ready:
                if(await self.can_cast(Orbital, SCANNERSWEEP_SCAN, Position)):
                    pass

    async def UseMule(self):
        """
        Call down mules
        """

        if(self.race == sc2.Race.Terran):
            for Orbital in self.units(ORBITALCOMMAND).ready:
                if(await self.can_cast(Orbital, CALLDOWNMULE_CALLDOWNMULE, )):
                    await self.do(Orbital.__call__(CALLDOWNMULE_CALLDOWNMULE, self.state.units(MINERALFIELD).closest_to(Orbital.position)))

    async def UseChronoboost(self):
        """

        """

        if(self.race == sc2.Race.Protoss):
            for Nexus in self.units(NEXUS).ready:
                pass

    async def UseLarvaInject(self):
        """

        """

        if(self.race == sc2.Race.Zerg):
            for Queen in self.units(QUEEN).ready:
                pass