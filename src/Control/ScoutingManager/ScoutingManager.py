import sc2
import numpy

from sc2.constants import *

import Utils.Drawing
import Utils.HelpFunctions as Help
import Data.GlobalData as Global

from Control.AbilityManager import AbilityManager as AbilityManager

# NOTE: Try to use this for every race!
class ScoutingManager(AbilityManager, Utils.Drawing):
    """
    This class handles the scouting of the enemy
    """

    def __init__(self):
        super().__init__()
    
        self.__ScoutIsNeeded = False
        self.__Scouts = list()
        self.__Point = 0
        self.__AbortScouting = False

    # Add documentation
    def Init(self):
        """
        Initialize the manager
        """

        super().Init()

        Help.PrintDebugMessage("Initialize ScoutingManager")

        if self.race == sc2.Race.Protoss:
            self.__Worker = PROBE
        elif self.race == sc2.Race.Terran:
            self.__Worker = SCV
        else:
            self.__Worker = DRONE

    def AddScout(self, Unit):
        """
        Add a new scout
        
        Arguments:
            Unit {sc.units.Unit} -- New scouting unit
        """

        if Unit not in self.__Scouts:
            self.__Scouts.append(Unit)
            Help.ReserveUnit(Unit)

    def RemoveScout(self, Unit):
        """
        Remove an existing scout
        
        Arguments:
            Unit {sc.units.Unit} -- Scouting unit
        """

        if Unit in self.__Scouts:
            self.__Scouts.remove(Unit)
            Help.FreeUnit(Unit)

    def SendScout(self):
        """
        Enable scouting
        """

        self.__ScoutIsNeeded = True

    def GetScouts(self):
        """
        Get the current scouts
        Returns:
            [list] -- List with scouts
        """

        return self.__Scouts

    async def ManagerIteration(self):
        """
        
        """

        await self.ScoutEnemy()
        await self.Intel()
        await super().ManagerIteration()

    async def Intel(self):
        """
        Process the intel of the bot
        """

        self.__UnitData = numpy.ones((self.game_info.map_size[1], self.game_info.map_size[0], 3), numpy.uint8)

        self.DrawOwnUnits(self.__UnitData)
        self.DrawEnemyBuildings(self.__UnitData, Global.SCOUTINGINFORMATIONS)
        self.DrawEnemyUnits(self.__UnitData)
        self.DrawImages({"Units:": self.__UnitData})

    async def ScoutEnemy(self, Position = None):
        """
        Scout the enemy with the given scouts

        Arguments: 
            Position {Point2D} -- Position for scouting (default: {None})
        """

        # Check if a new scout is needed
        if self.__ScoutIsNeeded:
            # Use a observer if a robotics is available
            if self.units(ROBOTICSFACILITY).ready:
                pass
            # Use an orbital command when ready
            elif self.units(ORBITALCOMMAND).ready:
                self.UseScan(Position)

                # Abort scouting if a scan is used
                return
            else:
                # Use a random worker as a scout
                self.AddScout(self.units(self.__Worker).tags_not_in(Global.RESERVEDUNITS["Worker"]).prefer_idle.filter(pred = lambda x: not(x.is_carrying_minerals or x.is_carrying_vespene))[0])

            self.__ScoutIsNeeded = False

        # Set the scouting route (circle around the enemy mainbase) if the given position is None
        if Position is None:
            Position = Help.CirclePoints(Center = self.enemy_start_locations[0], Point = self.__Point, Steps = 16)

        # Loop over each available scout
        for Scout in self.__Scouts:
            # Save all scouted informations
            Global.SCOUTINGINFORMATIONS.update({"Units": self.known_enemy_units})

            # Handle worker scouting
            if Scout.type_id == self.__Worker:
                # Check if the scout carry ressources
                # TODO: Verbessern
                # if(Scout.is_carrying_minerals or Scout.is_carrying_vespene):
                # print("[INFO] Scout carry ressources")
                # await self.do(Scout.__call__(sc2.ids.ability_id.HARVEST_RETURN_PROBE))

                # Check if the scout is dead
                if Scout.tag in self.state.dead_units:
                    self.RemoveScout(Scout)
    
                # TODO: Die HP aendern sich nicht...
                # Abort scouting if the unit got too much dmg
                elif self.__AbortScouting:
                    # Get the mineral positions next to the main base
                    Minerals = self.state.units(MINERALFIELD).closest_to(self.units(COMMANDCENTER).first.position)

                    # Remove the scout from the scout list, because he is send back to base
                    self.RemoveScout(Scout)

                    # Use mineralwalk to return
                    await self.do(Scout.gather(Minerals))
                else:
                    # Increase the circle counter
                    self.__Point += 1
                    
                    # Move the scout to the new position
                    await self.do(Scout.move(Position))

                # Check the enemy buildings
                # TODO: Teil mit Infos sammeln allgemein für jeden Scout nutzen
                # TODO: Hier ggf. für die Cheese-Detection ansetzen
                for Unit in self.known_enemy_units:
                    if Unit.type_id in Global.MAINBASES:
                        # Get the geyser around each command center
                        GasLeft = list()
                        for Geyser in self.state.units(VESPENEGEYSER).closer_than(10, Unit.position):
                            # Get the collected gas
                            GasLeft.append(Geyser.vespene_contents)

                        # Save the current gas count
                        Global.SCOUTINGINFORMATIONS.update({"GasLeft": GasLeft})

                    # TODO: Das hier ist unnoetig, sobald die HP des Scouts korrekt abgefragt werden kann
                    elif Unit.type_id == MARINE:
                        self.__AbortScouting = True

            # Handle observer scouting
            elif Scout.type_id == OBSERVER:
                pass

            # Handle overseer scouting
            elif Scout.type_id == OVERSEER:
                pass

            # Handle all other units
            else:
                pass
