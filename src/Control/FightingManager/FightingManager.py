import sc2
import numpy
import random

from sc2.constants import *

import Utils.HelpFunctions as Help
import Data.GlobalData as Global
from . import MicroManager as MM
from . import HarassManager as HM

# NOTE: Try to use this for every race!
class FightingManager(MM.MicroManager, HM.HarassManager):
    def __init__(self):        
        super().__init__()

        self.__DoSomethingAfter = 0

    def Init(self):
        """
        Initialize the manager
        """
        
        super().Init()

        Help.PrintDebugMessage("Initialize FightingManager")

    def FindTarget(self):
        if(len(self.known_enemy_units) > 0):
            return random.choice(self.known_enemy_units)
        elif(len(self.known_enemy_structures) > 0):
            return random.choice(self.known_enemy_structures)
        else:
            return self.enemy_start_locations[0]

    async def ManagerIteration(self):
        pass

    async def Attack(self):
        # Use this to create random decisions for the neural network
        if(len(self.units(STALKER).idle) > 0):
            Choice = random.randrange(0, 4)
            Target = False
            if(self.__Iteration > self.__DoSomethingAfter):
                # No attack
                if(Choice == 0):
                    self.__DoSomethingAfter = self.__Iteration + random.randrange(20, 165)

                # Attack the unit closest to a nexus
                elif(Choice == 1):
                    if(len(self.known_enemy_units) > 0):
                        Target = self.known_enemy_units.closest_to(random.choice(self.units(NEXUS)))

                # Attack enemy structures
                elif(Choice == 2):
                    if len(self.known_enemy_structures) > 0:
                        Target = random.choice(self.known_enemy_structures)

                # Attack_enemy_start
                elif(Choice == 3):
                    Target = self.enemy_start_locations[0]

                if(Target):
                    for Stalker in self.units(STALKER).idle:
                        await self.do(Stalker.attack(Target))

                y = numpy.zeros(4)
                y[Choice] = 1
                print(y)
                self.__TrainingData.append([y, self.__UnitData])