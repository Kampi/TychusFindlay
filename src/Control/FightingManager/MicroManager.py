import sc2

import Utils.HelpFunctions as Help

class MicroManager(sc2.BotAI):
    def __init__(self):
        super().__init__()

    def Init(self):
        """
        Initialize the manager
        """
        
        super().Init()

        Help.PrintDebugMessage("Initialize MicroManager")