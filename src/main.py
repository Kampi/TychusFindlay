import os
import time
import random
import argparse

import sc2
from sc2.constants import *
from sc2.player import Bot, Computer

import Control
import Callbacks
import Utils.HelpFunctions as Help

# Parse command line arguments
Parser = argparse.ArgumentParser()
Parser.add_argument("--realtime", help = "Run the game in real time", choices = ["yes", "no"], default = "no")
Parser.add_argument("--games", help = "Number of total games", type = int, default = 1)
Parser.add_argument("--enemy", help = "Choose a race for the enemy", choices = ["Terran", "Protoss", "Zerg"])
args = vars(Parser.parse_args())

class Tychus(
    Control.ScoutingManager,
    Control.ProductionManager,
    Control.FightingManager,
):
    def __init__(self):
        super().__init__()

        print("[GAME] Initialize bot...")

        Help.PrintDebugMessage("Debug mode enabled!")

    def on_start(self):
        # Initialize all subots
        super().Init()

    async def on_step(self, Iteration):
        """
        on_step callback handler
        
        Arguments:
            Iteration {int} -- Current game step
        """

        self.__Iteration = Iteration
        await self.distribute_workers()
        await self.CheckSupply()

        # Run a iteration with each manager
        await super().ManagerIteration()

    async def firstDepot(self):
        if self.__Iteration == 10:
            Worker = self.select_build_worker(pos=list(self.main_base_ramp.corner_depots)[0])
            print("going")
            await self.do(Worker.move(list(self.main_base_ramp.corner_depots)[0]))

    async def CheckSupply(self):
        """
        Check the supply
        """

        # Start scouting with 14 supply
        if (self.supply_used == 14) and not self.GetScouts():
            self.SendScout()

    async def offensive_force_buildings(self):
        if self.units(PYLON).ready.exists:
            Pylon = self.units(PYLON).ready.random

            if (self.units(GATEWAY).ready.exists) and (not self.units(CYBERNETICSCORE)):
                if not self.units(CYBERNETICSCORE):
                    if (self.can_afford(CYBERNETICSCORE)) and (not self.already_pending(CYBERNETICSCORE)):
                        await self.build(CYBERNETICSCORE, near=Pylon)
            elif len(self.units(GATEWAY)) < 3:
                if self.can_afford(GATEWAY) and (not self.already_pending(GATEWAY)):
                    await self.build(GATEWAY, near=Pylon)

            if (self.units(CYBERNETICSCORE).ready.exists) and not self.units(ROBOTICSFACILITY):
                if not self.units(ROBOTICSFACILITY):
                    if (self.can_afford(ROBOTICSFACILITY)) and (not self.already_pending(ROBOTICSFACILITY)):
                        await self.build(ROBOTICSFACILITY, near=Pylon)

    async def offensive_force(self):
        for Gateway in self.units(GATEWAY).ready.noqueue:
            if (self.can_afford(STALKER)) and (self.supply_left > 0):
                await self.do(Gateway.train(STALKER))

# Get a random difficulty level
Difficulty = random.randrange(0, 4)
DifficultyLevel = [sc2.Difficulty.VeryEasy, sc2.Difficulty.Easy, sc2.Difficulty.Medium, sc2.Difficulty.Hard]
print(f"[GAME] Difficulty: {DifficultyLevel[Difficulty]}")

# Get a random race if no race is given
if args["enemy"] is None:
    Race = random.randrange(0, 3)
    Races = [sc2.Race.Terran, sc2.Race.Zerg, sc2.Race.Protoss]
    ChosenRace = Races[Race]
# Otherwise pick the given race
else:
    if args["enemy"] == "Terran":
        ChosenRace = sc2.Race.Terran
    elif args["enemy"] == "Protoss":
        ChosenRace = sc2.Race.Protoss
    else:
        ChosenRace = sc2.Race.Zerg

print(f"[GAME] Enemy race: {ChosenRace}")

if __name__ == "__main__":
    for Game in range(args["games"]):
        # Run a starcraft 2 game with the given parameter
        GameResult = sc2.run_game(
            sc2.maps.get("AbyssalReefLE"),
            [Bot(sc2.Race.Terran, Tychus()), Computer(ChosenRace, DifficultyLevel[Difficulty])],
            realtime = Help.Str2Bool(args["realtime"]),
        )