import enum

class Cheese(enum.Enum):
    # Protoss
    CANONRUSH = 1
    OFFGATE = 2

    # Zerg
    PROXYHATCH = 3
    EARLYPOOL = 4

    # Terran
    OFFRAXS = 5
    
for Item in Cheese:
    assert not Item.name in globals()
    globals()[Item.name] = Item