import sc2

from sc2.constants import *

# Each unit and each building is encoded by a 8 bit RGB value
UNITDRAWINGS = {
    # Protoss
    # Set R = 0, G = 255 for protoss
    # Buildings
    NEXUS:                  [1, (0, 255, 8)],
    PYLON:                  [1, (0, 255, 16)],
    ASSIMILATOR:            [1, (0, 255, 24)],
    GATEWAY:                [1, (0, 255, 32)],
    WARPGATE:               [1, (0, 255, 40)],
    FORGE:                  [1, (0, 255, 48)],
    PHOTONCANNON:           [1, (0, 255, 56)],
    CYBERNETICSCORE:        [1, (0, 255, 64)],
    STARGATE:               [1, (0, 255, 72)],
    ROBOTICSFACILITY:       [1, (0, 255, 80)],
    TWILIGHTCOUNCIL:        [1, (0, 255, 88)],
    FLEETBEACON:            [1, (0, 255, 96)],
    ROBOTICSBAY:            [1, (0, 255, 104)],
    TEMPLARARCHIVE:         [1, (0, 255, 112)],
    DARKSHRINE:             [1, (0, 255, 120)],

    # Units
    PROBE:                  [1, (0, 255, 134)],
    MOTHERSHIP:             [1, (0, 255, 140)],
    ZEALOT:                 [1, (0, 255, 146)],
    SENTRY:                 [1, (0, 255, 152)],
    STALKER:                [1, (0, 255, 158)],
    HIGHTEMPLAR:            [1, (0, 255, 164)],
    DARKTEMPLAR:            [1, (0, 255, 170)],
    ARCHON:                 [1, (0, 255, 176)],
    ADEPT:                  [1, (0, 255, 182)],
    PHOENIX:                [1, (0, 255, 188)],
    ORACLE:                 [1, (0, 255, 194)],
    VOIDRAY:                [1, (0, 255, 200)],
    TEMPEST:                [1, (0, 255, 206)],
    CARRIER:                [1, (0, 255, 212)],
    OBSERVER:               [1, (0, 255, 218)],
    WARPPRISM:              [1, (0, 255, 224)],
    IMMORTAL:               [1, (0, 255, 230)],
    COLOSSUS:               [1, (0, 255, 236)],
    DISRUPTOR:              [1, (0, 255, 242)],

    # Terran
    # Set G = 0, B = 255 for terran
    # Buildings
    COMMANDCENTER:          [1, (5, 0, 255)],
    COMMANDCENTERFLYING:    [1, (10, 0, 255)],
    ORBITALCOMMAND:         [1, (15, 0, 255)],
    ORBITALCOMMANDFLYING:   [1, (20, 0, 255)],
    PLANETARYFORTRESS:      [1, (25, 0, 255)],
    BARRACKS:               [1, (30, 0, 255)],
    BARRACKSFLYING:         [1, (35, 0, 255)],
    BARRACKSTECHLAB:        [1, (40, 0, 255)],
    BARRACKSREACTOR:        [1, (45, 0, 255)],
    ENGINEERINGBAY:         [1, (50, 0, 255)],
    REFINERY:               [1, (55, 0, 255)],
    MISSILETURRET:          [1, (60, 0, 255)],
    SENSORTOWER:            [1, (65, 0, 255)],
    FACTORY:                [1, (70, 0, 255)],
    FACTORYFLYING:          [1, (75, 0, 255)],
    FACTORYTECHLAB:         [1, (80, 0, 255)],
    FACTORYREACTOR:         [1, (85, 0, 255)],
    BUNKER:                 [1, (90, 0, 255)],
    STARPORT:               [1, (95, 0, 255)],
    STARPORTFLYING:         [1, (100, 0, 255)],
    ARMORY:                 [1, (105, 0, 255)],
    FUSIONCORE:             [1, (110, 0, 255)],
    TECHLAB:                [1, (115, 0, 255)],
    REACTOR:                [1, (120, 0, 255)],

    # Units
    SCV:                    [1, (134, 0, 255)],
    MARINE:                 [1, (140, 0, 255)],
    MARAUDER:               [1, (146, 0, 255)],
    REAPER:                 [1, (152, 0, 255)],
    GHOST:                  [1, (158, 0, 255)],
    HELLION:                [1, (164, 0, 255)],
    SIEGETANK:              [1, (170, 0, 255)],
    SIEGETANKSIEGED:        [1, (176, 0, 255)],
    WIDOWMINE:              [1, (182, 0, 255)],
    #HELLBAT:                [1, (188, 0, 255)],
    THOR:                   [1, (194, 0, 255)],
    CYCLONE:                [1, (200, 0, 255)],
    VIKINGASSAULT:          [1, (206, 0, 255)],
    VIKINGFIGHTER:          [1, (212, 0, 255)],
    MEDIVAC:                [1, (218, 0, 255)],
    RAVEN:                  [1, (224, 0, 255)],
    BANSHEE:                [1, (230, 0, 255)],
    BATTLECRUISER:          [1, (236, 0, 255)],
    LIBERATOR:              [1, (242, 0, 255)],

    # Zerg
    # Set B = 0, R = 255 for zerg
    # Buildings
    HATCHERY:               [1, (255, 7, 0)],
    LAIR:                   [1, (255, 14, 0)],
    HIVE:                   [1, (255, 21, 0)],
    SPAWNINGPOOL:           [1, (255, 28, 0)],
    EVOLUTIONCHAMBER:       [1, (255, 35, 0)],
    ROACHWARREN:            [1, (255, 42, 0)],
    BANELINGNEST:           [1, (255, 49, 0)],
    SPINECRAWLER:           [1, (255, 56, 0)],
    SPORECRAWLER:           [1, (255, 63, 0)],
    INFESTATIONPIT:         [1, (255, 70, 0)],
    HYDRALISKDEN:           [1, (255, 77, 0)],
    LURKERDEN:              [1, (255, 84, 0)],
    SPIRE:                  [1, (255, 91, 0)],
    NYDUSNETWORK:           [1, (255, 98, 0)],
    ULTRALISKCAVERN:        [1, (255, 105, 0)],
    GREATERSPIRE:           [1, (255, 112, 0)],
    EXTRACTOR:              [1, (255, 119, 0)],

    # Units
    QUEEN:                  [1, (255, 133, 0)],
    DRONE:                  [1, (255, 138, 0)],
    MUTALISK:               [1, (255, 143, 0)],
    INFESTOR:               [1, (255, 148, 0)],
    SWARMHOSTMP:            [1, (255, 153, 0)],
    VIPER:                  [1, (255, 158, 0)],
    ULTRALISK:              [1, (255, 163, 0)],
    OVERLORD:               [1, (255, 168, 0)],
    ZERGLING:               [1, (255, 173, 0)],
    ROACH:                  [1, (255, 178, 0)],
    HYDRALISK:              [1, (255, 183, 0)],
    CORRUPTOR:              [1, (255, 188, 0)],
    OVERSEER:               [1, (255, 193, 0)],
    BANELING:               [1, (255, 198, 0)],
    RAVAGER:                [1, (255, 203, 0)],
    LURKER:                 [1, (255, 208, 0)],
    BROODLORD:              [1, (255, 213, 0)],
    NYDUSCANAL:             [1, (255, 218, 0)],
    BROODLING:              [1, (255, 223, 0)],
    #LARVA:                  [1, (255, 228, 0)],
    LOCUSTMPFLYING:         [1, (255, 233, 0)],
    LOCUSTMP:               [1, (255, 238, 0)],
    BANELINGCOCOON:         [1, (255, 243, 0)],
}

ITERATIONS_PER_MINUTE = 165
WORKER_PER_MINERALS = 16
MAX_WORKERS = 70
MAX_BASES = 3

MAINBASES = [NEXUS, COMMANDCENTER, HATCHERY]

SPLASH = [HELLION, HELLIONTANK, SIEGETANKSIEGED, PLANETARYFORTRESS, COLOSSUS, ARCHON, BANELING, ULTRALISK]

# Dictionary for reserved units
RESERVEDUNITS = {"Worker" : list(), "Ground" : list(), "Air" : list()}

# Dictionary with scouting informations
SCOUTINGINFORMATIONS = {"Units" : list()}

# Directories
ROOT_DIR = ""
TRAINING_DIR = "TrainingData"