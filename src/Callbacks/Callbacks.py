import os
import sc2
import time
import numpy

import Data.GlobalData as Global

class Callbacks(object):
    def __init__(self):
        pass

    def on_start(self):
        """
        On game start callback handler
        """

        print(f"[GAME] Start game...")

    def on_end(self, GameResult):
        """
        On game end callback handler
        Arguments:
            GameResult {gameresult.Result} -- Result of the match
        """

        print(f"[GAME] Game is over...")
        print(f"    Result: {GameResult}")

        if GameResult == sc2.Result.Victory:
            # Check if the output dir exist and create it if not
            if not os.path.exists(Global.ROOT_DIR + os.path.sep + Global.TRAINING_DIR):
                os.makedirs(Global.ROOT_DIR + os.path.sep + Global.TRAINING_DIR)

            # Write the data to the disk
            numpy.save(
                Global.ROOT_DIR
                + os.path.sep
                + Global.TRAINING_DIR
                + os.path.sep
                + "{}.npy".format(time.time()),
                numpy.array(self.__TrainingData),
            )

    def on_unit_destroyed(self, unit_tag):
        """
        Arguments:
            unit_tag {[type]} -- [description]
        """

        # Remove the unit from the reserved units if it got destroyed
        for Key in Global.RESERVEDUNITS:
            if unit_tag in Global.RESERVEDUNITS[Key]:
                Global.RESERVEDUNITS[Key].remove(unit_tag)

    def on_unit_created(self, unit):
        """
        Arguments:
            unit {[type]} -- [description]
        """

        pass

    def on_building_construction_complete(self, unit):
        """
        Arguments:
            unit {[type]} -- [description]
        """

        pass
