# ToDo

## Open

- [x] Erster Scout mit Mineralwalk nach Hause @Kampi
- [x] Keine Gebäude in Mineralline @tweakimp
- [x] Projekt in Module teilen @tweakimp
- [x] Konstanten in einzelne Datei ausgliedern und fürs ganze Projekt verwenden @Kampi
- [ ] Schalter für openCV Übersicht
- [ ] Auswerten des Scout-Ergebnisses (Cheese-Detection, etc.) @Kampi
- [ ] Scout-Worker und Bau-Worker soll Ressource vor dem scouten abliefern @Kampi
- [ ] Abbruchbedingung für Scout-Worker auf Verlust der HP setzen (funktioniert atm auf Grund der API noch nicht -> Fix kommt später) @Kampi
- [x] Umstellung auf Terraner
- [x] on_start zur Initialisierung des Bots verwenden @Kampi
- [ ] Callbacks in staticmethods umwandeln? @Kampi
- [x] Vererbung anders designen (ggf. die einzelnen Manager von Callback und sc2.BotAI ableiten und dann die Main von den Managern ableiten?) @ Kampi
- [ ] Implementierung des Micromanagers @tweakimp
- [x] Wie können SUPPLYDEPOT und SUPPLYDEPOTLOWER vernünftig gleichzeitig behandelt werden?
- [x] "directions" aus "MarineMicro.py" als allgemeine Funktion definieren
- [x] Neue Circle Funktion in "ScoutingManager" einbauen
- [ ] "use_stim" einheitenneutral in "AbilityManager" einfügen

## Errors

- [x] Gestorbener Scout wird weiter gesteuert und produziert Abilitiy.Move Fehlermeldungen @Kampi
- [x] Scout worker wird für Depot genutzt @Kampi
- [x] Buildorder funktioniert noch nicht korrekt

## Konzeptideen

- Eigene Einheiten von den gescouteten Gegnereinheiten abhängig machen
  - Regelmäßiges scouten
  - Gesehene Einheiten zählen und Typ bestimmten. Dann passende Gegenmaßnahmen einleiten
    - Ablauf:
    - Main trigger ScoutingManager -> Main gibt Scoutinfos an BuildManager, der entsprechend dem gesehenen reagiert ->
      Main gibt Units an de FightingManager weiter der die Einheitenkontrolle übernimmt
      (Ggf. mal als Graph darstellen?)