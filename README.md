# Tychus Findlay

Tychus Findlay is a terran starcraft 2 bot and is designed to play in the [Starcraft 2 AI Ladder](https://sc2ai.net/). He should use a mix between reinforcement learning and convolutional neural networks to make decissions for his style.

Our goal is to create a bot with a solid macro game. He should also use micro to perform very well unit trades. An additional milestone is to implement some ai techniques for a dynamic reaction of the bot to various sorts of cheese (i. e. canon rush).

Please check the official Blizzard [repository](https://github.com/Blizzard/s2client-proto) if you need some additional stuff or documentation.

Otherwise send an [e-mail](DanielKampert@kampis-elektroecke.de) if you have any questions.

## Table of Contents

- [Tychus Findlay](#tychus-findlay)
    - [Table of Contents](#table-of-contents)
    - [Status of the project](#status-of-the-project)
    - [Prepare your system](#prepare-your-system)
    - [Use it](#use-it)
    - [The bot in action](#the-bot-in-action)
        - [Macro example](#macro-example)
        - [Micro example](#micro-example)
    - [History](#history)
    - [Maintainer](#maintainer)

## Status of the project

__Under construction__

A basic software concept, based on different kinds of game manager, is developed.

![Concept](doc/images/Concept.png)

## Prepare your system

- Install [Starcraft 2](https://starcraft2.com/de-de/)
- Install [Python](https://www.python.org/downloads/) (Version > 3.6)
- Install [Git](https://git-scm.com/downloads) and clone the repository
- Install necessary python modules `$ python -m pip install -r requirements.txt`

> NOTE:  
> You can install python-sc2 directly via pip. But make sure that you
use the current version of the module! The best solution is  to install the module with the URL of the [repository](https://github.com/Dentosal/python-sc2). 

## Use it

TBD

## The bot in action

On the following you can see some example plays of the bot.

### Macro example

TBD

### Micro example

30 Banelings with Speed and on creep vs. 25 Marines with Combat Shield and Stimpack.

[![Micro example](http://img.youtube.com/vi/498kz8kYoac/0.jpg)](https://youtu.be/498kz8kYoac "Micro example 1")

3 Ultralisk on creep vs. 25 Marines with Combat Shield and Stimpack.

[![Micro example](http://img.youtube.com/vi/eEuNSgDoOtE/0.jpg)](https://youtu.be/eEuNSgDoOtE "Micro example 2")

## History

| **Version**   | **Description**                                | **Date**       |
|:---------:|:------------------------------------------:|:----------:|
| 0.1a      | First macro release                        |  29.05.18  |

## Maintainer

- [Daniel Kampert](@Kampi)
- [Franz Weitkamp](@tweakimp)
- [Marian Sauter]()